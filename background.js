function sendMessageToTabs(tabs, data) {
  for (let tab of tabs) {
    browser.tabs
      .sendMessage(tab.id, { greeting: "Hi from background script", data })
      .then((response) => {
        console.log("Message from the content script:");
        console.log(response.response);
      })
      .catch((error) => {
        console.log(`Error: ${error}`);
      });
  }
}

const indexOfEnd = (mainString, subString) => {
  var io = mainString.indexOf(subString);
  return io == -1 ? -1 : io + subString.length;
};

function listener(details) {
  let filter = browser.webRequest.filterResponseData(details.requestId);
  let decoder = new TextDecoder("utf-8");
  let encoder = new TextEncoder();

  filter.ondata = (event) => {
    let str = decoder.decode(event.data, { stream: true });
    const data = JSON.parse(str);
    browser.tabs
      .query({
        currentWindow: true,
        active: true,
      })
      .then((tabs) => sendMessageToTabs(tabs, data))
      .catch((error) => {
        console.log(`Error: ${error}`);
      });

    filter.write(encoder.encode(str));
    filter.disconnect();
  };

  return {};
}

browser.webRequest.onBeforeRequest.addListener(
  listener,
  { urls: ["https://gitlab.com/api/graphql"] },
  ["blocking", "requestBody"]
);
