function sendMessageToTabs(tabs, data) {
  for (let tab of tabs) {
    browser.tabs
      .sendMessage(tab.id, { greeting: "Hi from background script", data })
      .then((response) => {
        console.log("Message from the content script:");
        console.log(response.response);
      })
      .catch((error) => {
        console.log(`Error: ${error}`);
      });
  }
}

const indexOfEnd = (mainString, subString) => {
  var io = mainString.indexOf(subString);
  return io == -1 ? -1 : io + subString.length;
};

function listener(details) {
  console.log("details", details);
  let filter = browser.webRequest.filterResponseData(details.requestId);
  let decoder = new TextDecoder("utf-8");
  let encoder = new TextEncoder();

  const decodedRequestBody = decoder.decode(details.requestBody.raw[0].bytes, {
    stream: true,
  });

  const indexForInjection = indexOfEnd(
    decodedRequestBody,
    "fragment IssueNode on Issue {\\n  "
  );
  console.log("REQUEST BODY", indexForInjection, decodedRequestBody);

  if (indexForInjection >= 0) {
    const newRequestBody =
      decodedRequestBody.slice(0, indexForInjection) +
      "closedAt\\n " +
      decodedRequestBody.slice(indexForInjection);
    console.log("NEW REQUEST BODY", indexForInjection, newRequestBody);
    details.requestBody.raw[0].bytes = encoder.encode(newRequestBody);
  }

  filter.ondata = (event) => {
    let str = decoder.decode(event.data, { stream: true });
    // Just change any instance of Example in the HTTP response
    // to WebExtension Example.
    //   str = str.replace(/Example/g, 'WebExtension Example');
    // console.log("ONDATA", str);

    const data = JSON.parse(str);
    browser.tabs
      .query({
        currentWindow: true,
        active: true,
      })
      .then((tabs) => sendMessageToTabs(tabs, data))
      .catch((error) => {
        console.log(`Error: ${error}`);
      });

    filter.write(encoder.encode(str));
    filter.disconnect();
  };

  return { requestBody: details.requestBody };
}

browser.webRequest.onBeforeRequest.addListener(
  listener,
  { urls: ["https://gitlab.com/api/graphql"] },
  ["blocking", "requestBody"]
);
