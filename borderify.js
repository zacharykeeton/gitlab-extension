let fetched = false;

// CSS
//// create and inject css rules into the page so that our
//// fireworks will appear
const sheet = window.document.styleSheets[0];
sheet.insertRule(
  `
    li.board-card.newly-closed { 
        background-color: black;
        color: white;
        background-image: url(${browser.extension.getURL(
          "images/fireworks.gif"
        )}); 
    }`,
  sheet.cssRules.length
);
sheet.insertRule(
  `
    li.board-card.newly-closed span.board-card-number, li.board-card.newly-closed h4.board-card-title a {
          color:white;
    }`,
  sheet.cssRules.length
);

// hack sleep. TODO improve & remove
function sleep(ms, payload) {
  return new Promise(resolve => setTimeout(()=>resolve(payload), ms));
}

// Figure out how many days worth of issues to fetch
// On 'not Monday' just get the past 1 day.  
// On Mondays get the past three days to account for the weekend
// and get the issues closed since last Friday.
const isMonday = new Date().getDay() === 1;
const daysAgo = isMonday ? 3 : 1;

const priorDay = new Date();
priorDay.setDate(priorDay.getDate() - daysAgo);

browser.runtime.onMessage.addListener((request) => {
  let idDataString;
  let apiUri;
  let id;

  // Handle project-level boards
  const project = request.data.data?.project;
  if (project) {
    idDataString = project.id;
    const splitProjectString = idDataString.split("/");
    id = splitProjectString[splitProjectString.length - 1];
    apiUri = `https://gitlab.com/api/v4/projects/${id}/issues?state=closed&updated_after=${priorDay.toISOString()}`;
  }

  // handle group-level boards
  const group = request.data.data?.group;
  if (group) {
    idDataString = group.id;
    const splitProjectString = idDataString.split("/");
    id = splitProjectString[splitProjectString.length - 1];
    apiUri = `https://gitlab.com/api/v4/groups/${id}/issues?state=closed&updated_after=${priorDay.toISOString()}`;
  }

  if (!id || !apiUri)
    return Promise.reject(
      `Could not fetch data. idToUse: ${idDataString}, apiUri: ${apiUri}`
    );

  // Use 'fetched' as state so that we only do the API calls once
  if (!fetched) {
    fetched = true;
    fetch(apiUri)
      .then((response) => response.json())
      .then(issues => sleep(1000, issues))
      .then((issues) => {
        issues.forEach(({ iid , closed_at}) => {
          // For each fetched issue: see 1 & 2 below.

          // 1) grab it's dom element
          const issueElement = document.querySelector(
            `[data-item-iid='${iid}']`
          );

          if(issueElement){
            // 2) if that issue has been closed within the 
            // calculated time frame (24 hours unless today is Monday, then 72),
            // add the CSS class to show fireworks.
            const timeframeHours = isMonday ? 72 : 24
            const hoursSinceIssueWasClosed = Math.abs(new Date() - new Date(closed_at)) / 36e5;
            console.log('hsc', hoursSinceIssueWasClosed)
            const closedWithinTimeframe = hoursSinceIssueWasClosed <= timeframeHours;
            closedWithinTimeframe && issueElement.classList.add("newly-closed");
          }
        });
      })
      .catch((error) => console.log(error));
  }
  return Promise.resolve({ response: "Hi from content script" });
});
